package com.syltech.edrum.Eardrum_Main;

/**
 * Created by nigma on 03/03/2018.
 */

import java.io.IOException;
import android.media.MediaRecorder;

public class DetectNoise {
    // This file is used to record voice
     static final public double EMA_FILTER = 0.6;

    public MediaRecorder mRecorder = null;
    public double mEMA = 0.0;

    //start detecting sound
    public void start() {

        if (mRecorder == null) {

            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mRecorder.setOutputFile("/dev/null");

            try {
                mRecorder.prepare();
                mRecorder.start();
                mEMA = 0.0;
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        }
    }

    //stop detecting sound
    public void stop() {
        if (mRecorder != null) {
            try {
                mRecorder.stop();
                mRecorder.release();
                mRecorder = null;
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
    }

    //Amplitude,
    //the maximum displacement or distance moved by a point
    // on a vibrating body or wave measured from its equilibrium position.
    public double getAmplitude() {
        double db=0;
        if (mRecorder != null)
            db=20 * Math.log10(mRecorder.getMaxAmplitude() / 2700.0);
            if (db > 0) {
                return db;
            }
        else
            return 0;

    }

    public double getAmplitudeEMA() {
        double amp = getAmplitude();
        mEMA = EMA_FILTER * amp + (1.0 - EMA_FILTER) * mEMA;
        return mEMA;
    }
}
