package com.syltech.edrum;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.syltech.edrum.Fragments.AlerterFrag;
import com.syltech.edrum.Fragments.SpeechFrag;
import com.syltech.edrum.Service.EardrumService;

import static com.syltech.edrum.PermissionHub.PermissionHandler.CALL_PHONE;
import static com.syltech.edrum.PermissionHub.PermissionHandler.RECORD_AUDIO;

public class Home extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    private TextView mTextMessage;

    private Toolbar toolbar;


    BottomNavigationView navigationB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                    RECORD_AUDIO);
        }

        if (ActivityCompat.checkSelfPermission(Home.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},
                    CALL_PHONE);
        }

        navigationB = (BottomNavigationView) findViewById(R.id.navigation);

        toolbar =(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        toolbar.setTitle("eardrum");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorBlue));


        navigationB.setOnNavigationItemSelectedListener(this);

        setDefaultFragment();
    }

    private void setDefaultFragment() {
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, SpeechFrag.newInstance());
        transaction.commit();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment selectedFragment=null;
        switch (item.getItemId()) {
            case R.id.action_s2t:
                selectedFragment=SpeechFrag.newInstance();
                break;
            case R.id.action_alerter:
                selectedFragment= AlerterFrag.newInstance();
                break;

        }

        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout,selectedFragment);
        transaction.commit();
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();

        Intent intent=new Intent(this,EardrumService.class);
        startService(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent=new Intent(this,EardrumService.class);
        startService(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.navigation_info){
            infoDialog();
        }
        return super.onOptionsItemSelected(item);
    }
    public void infoDialog(){
        Dialog dialog=new Dialog(Home.this);
        dialog.setContentView(R.layout.about_app);
        final TextView txtPhone=dialog.findViewById(R.id.textView4);

        dialog.setCancelable(true);
        dialog.show();



        txtPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + txtPhone.getText().toString()));
                if (ActivityCompat.checkSelfPermission(Home.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(Home.this, new String[]{Manifest.permission.CALL_PHONE},
                            CALL_PHONE);
                }

                startActivity(intent);
            }
        });
    }


}
