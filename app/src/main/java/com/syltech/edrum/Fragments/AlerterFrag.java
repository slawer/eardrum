package com.syltech.edrum.Fragments;


import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.syltech.edrum.Eardrum_Main.DetectNoise;
import com.syltech.edrum.R;
import com.syltech.edrum.Service.EardrumService;

public class AlerterFrag extends Fragment implements View.OnClickListener {

    View myFragment;

    /* constants */
    private static final int POLL_INTERVAL = 300;

    /** running state **/
    private static boolean mRunning = false;

    /** config state **/
    private static int mThreshold;

    int RECORD_AUDIO = 0;
    private static PowerManager.WakeLock mWakeLock;

    private static Handler mHandler = new Handler();

    /* References to view elements */
    private static TextView mStatusView,tv_noice;

    private static ToggleButton tgbState;

    /* sound data source */
    private static DetectNoise mSensor;
    static ProgressBar  bar;

    private Button button;

    private TextView txtalmsg;
    private TextView txtstatsmg;


    private  Runnable mSleepTask = new Runnable() {
        public void run() {
            //Log.i("Noise", "runnable mSleepTask");
            start();
        }
    };

    // Create runnable thread to Monitor Sound
    private  Runnable mPollTask = new Runnable() {
        public void run() {
            double amp = mSensor.getAmplitude();

            //updateDisplay("Listening...", amp);

            if ((amp >= 18&&(amp <= 30))) {
                //alertUser(amp);
                alert();
                stop();
                updateDisplay("Stopped...", amp);
                txtstatsmg.setText(R.string.toggleOffMsg);
                txtstatsmg.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                tgbState.setChecked(false);
                //Log.i("Noise", "==== onCreate ===");
            }

            // Runnable(mPollTask) will again execute after POLL_INTERVAL
            mHandler.postDelayed(mPollTask, POLL_INTERVAL);
        }
    };


    public static AlerterFrag newInstance() {
        AlerterFrag fragment = new AlerterFrag();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout and views for this fragment
        myFragment=inflater.inflate(R.layout.fragment_alerter,container,false);
        mStatusView = myFragment.findViewById(R.id.status);
        txtstatsmg = myFragment.findViewById(R.id.txtstatsmg);
        txtalmsg = myFragment.findViewById(R.id.txtalmsg);
        tv_noice = myFragment.findViewById(R.id.tv_noice);
        bar = myFragment.findViewById(R.id.progressBar1);
        tgbState = myFragment.findViewById(R.id.toggleButton);


        // Used to listen
        mSensor = new DetectNoise();
        PowerManager pm = (PowerManager) getActivity().getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "NoiseAlert");

        tgbState.setOnClickListener(this);


        return myFragment;
    }



    @Override
    public void onPause() {
        super.onPause();

        // To prevent starting the service if the required permission is NOT granted.
    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == DRAW_OVER_OTHER_APP_PERMISSION) {
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                if (!Settings.canDrawOverlays(getActivity())) {
//                    //Permission is not available. Display error text.
//                    errorToast();
//                    getActivity().finish();
//                }
//            }
//        } else {
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//    }

//    private void errorToast() {
//        Toast.makeText(getActivity(), "Draw over other app permission not available. Can't start the application without the permission.", Toast.LENGTH_LONG).show();
//    }



    @Override
    public void onClick(View v) {
        if(tgbState.isChecked()){
            //Toast.makeText(getActivity(), "Toggle On", Toast.LENGTH_SHORT).show();
            txtstatsmg.setText(R.string.toggleOnMsg);
            txtstatsmg.setTextColor(getResources().getColor(android.R.color.holo_green_dark));

            start();

            mStatusView.setText("Listening...");
        }

        else {
            //Toast.makeText(getActivity(), "Toggle Off", Toast.LENGTH_SHORT).show();
            txtstatsmg.setText(R.string.toggleOffMsg);
            stop();
            txtstatsmg.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            Intent intent=new Intent(getActivity(),EardrumService.class);
            getActivity().stopService(intent);

            //getActivity().stopService(new Intent(getActivity(), FloatingWidgetService.class));
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        // Log.i("Noise", "==== onStop ===");
        //Stop noise monitoring
        //stop();
//        Intent intent=new Intent(getActivity(),EardrumService.class);
//        getActivity().startService(intent);
    }

    public void start() {

        //Log.i("Noise", "==== start ===");
        mSensor.start();
        if (!mWakeLock.isHeld()) {
            mWakeLock.acquire();
        }
        //Noise monitoring start
        // Runnable(mPollTask) will execute after POLL_INTERVAL
        mHandler.postDelayed(mPollTask, POLL_INTERVAL);
    }
    public void stop() {
        Log.d("Noise", "==== Stop Noise Monitoring===");
        if (mWakeLock.isHeld()) {
            mWakeLock.release();
        }
        mHandler.removeCallbacks(mSleepTask);
        mHandler.removeCallbacks(mPollTask);
        mSensor.stop();
        bar.setProgress(0);
        updateDisplay("Stopped...", 0.0);
        mRunning = false;

    }


    private  void updateDisplay(String status, double signalEMA) {
        mStatusView.setText(status);
        //
        bar.setProgress((int)signalEMA);
        Log.d("SOUND", String.valueOf(signalEMA));
        tv_noice.setText(signalEMA+"dB");
    }




    public void alert(){
        //To set large icon notifcation
        Bitmap icon1= BitmapFactory.decodeResource(getActivity().getResources(),R.mipmap.ic_launcher);
        //Assign BigText style notification
        NotificationCompat.BigTextStyle bigText= new NotificationCompat.BigTextStyle();
        bigText.bigText(getActivity().getResources().getString(R.string.notificationMsg));
        bigText.setBigContentTitle(getString(R.string.eardrum_alert));
        //build notification
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getActivity())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.eardrum_alert))
                .setLargeIcon(icon1)
                .setStyle(bigText);
        mBuilder.setLights(0xff00ff00, 300, 100);
        Uri alarmsound= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmsound);
        mBuilder.setVibrate(new long[]{1000,3000,1000});

        NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, mBuilder.build());
    }
}
