package com.syltech.edrum.Fragments;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.syltech.edrum.PermissionHub.PermissionHandler;
import com.syltech.edrum.R;
import com.syltech.edrum.Eardrum_Main.SpeechRecognizerManager;

import java.util.ArrayList;


public class SpeechFrag extends Fragment implements View.OnClickListener{
    View myFragment;

    private TextView result_tv;
    private TextView txtStart,txtStop;
    private ImageButton btnStartListener;
    private ImageButton btnStopListener;
    private Button start_listen_btn;
    private Button stop_listen_btn,mute;
    private SpeechRecognizerManager mSpeechManager;

    public static SpeechFrag newInstance() {
        SpeechFrag fragment = new SpeechFrag();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        myFragment=inflater.inflate(R.layout.fragment_speech,container,false);

        result_tv = myFragment.findViewById(R.id.result_tv);
        txtStart = myFragment.findViewById(R.id.textViewStart);
        txtStop = myFragment.findViewById(R.id.textViewStop);
        btnStartListener = myFragment.findViewById(R.id.imgStartListener);
        btnStopListener = myFragment.findViewById(R.id.imgStopListener);
        mute = myFragment.findViewById(R.id.mute);

        btnStartListener.setOnClickListener(this);
        btnStopListener.setOnClickListener(this);
        mute.setOnClickListener(this);
        return myFragment;
    }

    @Override
    public void onClick(View v) {

        if(PermissionHandler.checkPermission(getActivity(),PermissionHandler.RECORD_AUDIO)) {

            switch (v.getId()) {
                case R.id.imgStartListener:
                    if(mSpeechManager==null)
                    {
                        SetSpeechListener();
                        btnStartListener.setVisibility(View.INVISIBLE);
                        txtStart.setVisibility(View.INVISIBLE);
                        btnStopListener.setVisibility(View.VISIBLE);
                        txtStop.setVisibility(View.VISIBLE);
                    }
                    else if(!mSpeechManager.ismIsListening())
                    {
                        mSpeechManager.destroy();
                        SetSpeechListener();
                        btnStartListener.setVisibility(View.INVISIBLE);
                        txtStart.setVisibility(View.INVISIBLE);
                        btnStopListener.setVisibility(View.VISIBLE);
                        txtStop.setVisibility(View.VISIBLE);
                    }
                    result_tv.setText(getString(R.string.you_may_speak));

                    break;
                case R.id.imgStopListener:
                    if(mSpeechManager!=null) {
                        result_tv.setText(getString(R.string.destroyed));
                        mSpeechManager.destroy();
                        mSpeechManager = null;
                        btnStartListener.setVisibility(View.VISIBLE);
                        txtStart.setVisibility(View.VISIBLE);
                        btnStopListener.setVisibility(View.INVISIBLE);
                        txtStop.setVisibility(View.INVISIBLE);
                    }
                    break;
                case R.id.mute:
                    if(mSpeechManager!=null) {
                        if(mSpeechManager.isInMuteMode()) {
                            mute.setText(getString(R.string.mute));
                            mSpeechManager.mute(false);
                        }
                        else
                        {
                            mute.setText(getString(R.string.un_mute));
                            mSpeechManager.mute(true);
                        }
                    }
                    break;
            }
        }
        else
        {
            PermissionHandler.askForPermission(PermissionHandler.RECORD_AUDIO,getActivity());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode)
        {
            case PermissionHandler.RECORD_AUDIO:
                if(grantResults.length>0) {
                    if(grantResults[0]== PackageManager.PERMISSION_GRANTED) {
                        start_listen_btn.performClick();
                    }
                }
                break;

        }
    }

    private void SetSpeechListener()
    {
        mSpeechManager=new SpeechRecognizerManager(getActivity(), new SpeechRecognizerManager.onResultsReady() {
            @Override
            public void onResults(ArrayList<String> results) {



                if(results!=null && results.size()>0)
                {

                    if(results.size()==1)
                    {
                        mSpeechManager.destroy();
                        mSpeechManager = null;
                        result_tv.setText(results.get(0));
                    }
                    else {
                        StringBuilder sb = new StringBuilder();
                        if (results.size() > 5) {
                            results = (ArrayList<String>) results.subList(0, 5);
                        }
                        for (String result : results) {
                           // sb.append(result).append("\n");

                        result_tv.setText(result);
                    }}
                }
                else
                    result_tv.setText(getString(R.string.no_results_found));
            }
        });
    }

    @Override
    public void onPause() {
        if(mSpeechManager!=null) {
            mSpeechManager.destroy();
            mSpeechManager=null;
        }
        super.onPause();
    }
}
