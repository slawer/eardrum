package com.syltech.edrum.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.syltech.edrum.R;

/**
 * Created by nigma on 15/03/2018.
 */

public class SliderAdapter extends PagerAdapter{
    Context context;
    LayoutInflater layoutInflater;

    public SliderAdapter(Context context) {
        this.context = context;
    }

    public int[] slide_image={
            R.drawable.group10,
            R.drawable.group11,
            R.drawable.group12
    };

    public String[] slide_headings={
            "EAT",
            "SLEEP",
            "CODE"
    };

    public String[] slide_descrip={
            "ajnsdjkanjksndjkanskjdnkajsnjknaskjdnakjsndjaksdkaksndakdaksdkaskdaksdknjasnjkdn",
            "ajnsdjkanjksndjkanskjdnkajsnjknaskjdnakjsndjaksdkaksndakdaksdkaskdaksdknjasnjkdn",
            "ajnsdjkanjksndjkanskjdnkajsnjknaskjdnakjsndjaksdkaksndakdaksdkaskdaksdknjasnjkdn"
    };

    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view== object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=layoutInflater.inflate(R.layout.slide_layout,container,false);

        ImageView slideImageView = view.findViewById(R.id.slide_image);
        TextView slideHeader = view.findViewById(R.id.slide_header);
        TextView slideDescription = view.findViewById(R.id.slide_description);

        slideImageView.setImageResource(slide_image[position]);
        slideHeader.setText(slide_headings[position]);
        slideDescription.setText(slide_descrip[position]);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((RelativeLayout)object);

    }
}
