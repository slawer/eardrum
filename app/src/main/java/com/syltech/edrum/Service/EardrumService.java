package com.syltech.edrum.Service;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.syltech.edrum.Eardrum_Main.DetectNoise;
import com.syltech.edrum.Fragments.AlerterFrag;
import com.syltech.edrum.Home;
import com.syltech.edrum.R;

public class EardrumService extends Service {

    /* constants */
    private static final int POLL_INTERVAL = 300;

    /** running state **/
    private static boolean mRunning = false;

    /** config state **/
    private static int mThreshold;

    int RECORD_AUDIO = 0;
    private static PowerManager.WakeLock mWakeLock;

    private static Handler mHandler = new Handler();

    /* References to view elements */
    private static TextView mStatusView,tv_noice;

    private static ToggleButton tgbState;

    /* sound data source */
    private static DetectNoise mSensor;
    static ProgressBar bar;

    private Button button;

    private TextView txtalmsg;
    private TextView txtstatsmg;

    public AlerterFrag alerterFrag;


    private  Runnable mSleepTask = new Runnable() {
        public void run() {
            //Log.i("Noise", "runnable mSleepTask");
            start();
        }
    };


    // Create runnable thread to Monitor Sound
    private  Runnable mPollTask = new Runnable() {
        public void run() {
            double amp = mSensor.getAmplitude();

            //updateDisplay("Listening...", amp);

            if ((amp >= 18&&(amp <= 30))) {
                //alertUser(amp);
                alert();
                stop();
                //updateDisplay("Stopped...", amp);
                //txtstatsmg.setText(R.string.toggleOffMsg);
                //txtstatsmg.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                //tgbState.setChecked(false);
                //Log.i("Noise", "==== onCreate ===");
            }

            // Runnable(mPollTask) will again execute after POLL_INTERVAL
            mHandler.postDelayed(mPollTask, POLL_INTERVAL);
        }
    };

    public EardrumService(){

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        mSensor=new DetectNoise();
        PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "NoiseAlert");
        start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
//        stop();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void start() {

        //Log.i("Noise", "==== start ===");
        mSensor.start();
        if (!mWakeLock.isHeld()) {
            mWakeLock.acquire();
        }
        //Noise monitoring start
        // Runnable(mPollTask) will execute after POLL_INTERVAL
        mHandler.postDelayed(mPollTask, POLL_INTERVAL);
    }
    public void stop() {
        Log.d("Noise", "==== Stop Noise Monitoring===");
        if (mWakeLock.isHeld()) {
            mWakeLock.release();
        }
        mHandler.removeCallbacks(mSleepTask);
        mHandler.removeCallbacks(mPollTask);
        mSensor.stop();
        //bar.setProgress(0);
        //updateDisplay("Stopped...", 0.0);
        mRunning = false;

    }

    public void alert(){
        Intent intent=new Intent(EardrumService.this, Home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //To set large icon notifcation
        Bitmap icon1= BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher);

        PendingIntent pendingIntent=PendingIntent.getActivity(EardrumService.this,0,intent,PendingIntent.FLAG_ONE_SHOT);

        //Assign BigText style notification
        NotificationCompat.BigTextStyle bigText= new NotificationCompat.BigTextStyle();
        bigText.bigText(this.getResources().getString(R.string.notificationMsg));
        bigText.setBigContentTitle(getString(R.string.eardrum_alert));
        //build notification
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this
        )
                .setSmallIcon(R.drawable.ic_hearing_black_24dp)
                .setContentTitle(getString(R.string.eardrum_alert))
                .setLargeIcon(icon1)
                .setStyle(bigText)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);
        mBuilder.setLights(0xff00ff00, 300, 100);
        Uri alarmsound= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmsound);
        mBuilder.setVibrate(new long[]{1000,3000,1000});

        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, mBuilder.build());
    }


}
