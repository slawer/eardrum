package com.syltech.edrum;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class Splash extends AppCompatActivity {

    private ImageView imgLogo;
    private TextView txtLogoText;

    Animation animation;

    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        imgLogo= (ImageView) findViewById(R.id.imageView2);
        txtLogoText= (TextView) findViewById(R.id.textView8);

        animation= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.push_down);
        imgLogo.setAnimation(animation);

        animation= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.push_right);
        txtLogoText.setAnimation(animation);

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Splash.this, Home.class);
                startActivity(intent);
                finish();
            }
        }, 4000);
    }
}
